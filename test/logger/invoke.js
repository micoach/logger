'use strict';

const Code = require('code');
const Lab = require('lab');
const lab = exports.lab = Lab.script();
const { describe, it, beforeEach } = lab;

const { expect } = Code;

const { assert, stub } = require('sinon');

const Logger = require('../..')({ file: false });

describe('Logger invocation', () => {

    const suite = {};

    describe('without context name', () => {

        beforeEach(done => {
            suite.logger = Logger();
            done();
        });

        it('exposes full logger api', done => {
            expect(suite.logger.fatal).to.be.a.function();
            expect(suite.logger.error).to.be.a.function();
            expect(suite.logger.warn).to.be.a.function();
            expect(suite.logger.info).to.be.a.function();
            expect(suite.logger.debug).to.be.a.function();
            expect(suite.logger.trace).to.be.a.function();
            done();
        });

        it('delegates fatal to logger instance', done => {
            stub(suite.logger.defaultLogger, 'fatal');
            suite.logger.fatal(123);
            assert.calledWith(suite.logger.defaultLogger.fatal, 123);
            suite.logger.defaultLogger.fatal.restore();
            done();
        });

        it('delegates error to logger instance', done => {
            stub(suite.logger.defaultLogger, 'error');
            suite.logger.error(123);
            assert.calledWith(suite.logger.defaultLogger.error, 123);
            suite.logger.defaultLogger.error.restore();
            done();
        });

        it('delegates warn to logger instance', done => {
            stub(suite.logger.defaultLogger, 'warn');
            suite.logger.warn(123);
            assert.calledWith(suite.logger.defaultLogger.warn, 123);
            suite.logger.defaultLogger.warn.restore();
            done();
        });

        it('delegates info to logger instance', done => {
            stub(suite.logger.defaultLogger, 'info');
            suite.logger.info(123);
            assert.calledWith(suite.logger.defaultLogger.info, 123);
            suite.logger.defaultLogger.info.restore();
            done();
        });

        it('delegates debug to logger instance', done => {
            stub(suite.logger.defaultLogger, 'debug');
            suite.logger.debug(123);
            assert.calledWith(suite.logger.defaultLogger.debug, 123);
            suite.logger.defaultLogger.debug.restore();
            done();
        });

        it('delegates trace to logger instance', done => {
            stub(suite.logger.defaultLogger, 'trace');
            suite.logger.trace(123);
            assert.calledWith(suite.logger.defaultLogger.trace, 123);
            suite.logger.defaultLogger.trace.restore();
            done();
        });

    });

    describe('with context name provided', () => {

        beforeEach(done => {
            suite.logger = Logger('context-name');
            done();
        });

        it('exposes full logger api', done => {
            expect(suite.logger.fatal).to.be.a.function();
            expect(suite.logger.error).to.be.a.function();
            expect(suite.logger.warn).to.be.a.function();
            expect(suite.logger.info).to.be.a.function();
            expect(suite.logger.debug).to.be.a.function();
            expect(suite.logger.trace).to.be.a.function();
            done();
        });
    });

    describe('with configuration provided', () => {

        describe('without throttling option', () => {

            beforeEach(done => {
                const config = {
                    name: 'LOG_DEFAULT_NAME'
                };
                suite.logger = Logger(config);
                done();
            });

            it('exposes full logger api', done => {
                expect(suite.logger.fatal).to.be.a.function();
                expect(suite.logger.error).to.be.a.function();
                expect(suite.logger.warn).to.be.a.function();
                expect(suite.logger.info).to.be.a.function();
                expect(suite.logger.debug).to.be.a.function();
                expect(suite.logger.trace).to.be.a.function();
                done();
            });

            it('delegates fatal to logger instance', done => {
                stub(suite.logger.defaultLogger, 'fatal');
                suite.logger.fatal(123);
                assert.calledWith(suite.logger.defaultLogger.fatal, 123);
                suite.logger.defaultLogger.fatal.restore();
                done();
            });

            it('delegates error to logger instance', done => {
                stub(suite.logger.defaultLogger, 'error');
                suite.logger.error(123);
                assert.calledWith(suite.logger.defaultLogger.error, 123);
                suite.logger.defaultLogger.error.restore();
                done();
            });

            it('delegates warn to logger instance', done => {
                stub(suite.logger.defaultLogger, 'warn');
                suite.logger.warn(123);
                assert.calledWith(suite.logger.defaultLogger.warn, 123);
                suite.logger.defaultLogger.warn.restore();
                done();
            });

            it('delegates info to logger instance', done => {
                stub(suite.logger.defaultLogger, 'info');
                suite.logger.info(123);
                assert.calledWith(suite.logger.defaultLogger.info, 123);
                suite.logger.defaultLogger.info.restore();
                done();
            });

            it('delegates debug to logger instance', done => {
                stub(suite.logger.defaultLogger, 'debug');
                suite.logger.debug(123);
                assert.calledWith(suite.logger.defaultLogger.debug, 123);
                suite.logger.defaultLogger.debug.restore();
                done();
            });

            it('delegates trace to logger instance', done => {
                stub(suite.logger.defaultLogger, 'trace');
                suite.logger.trace(123);
                assert.calledWith(suite.logger.defaultLogger.trace, 123);
                suite.logger.defaultLogger.trace.restore();
                done();
            });

        });
        describe('with throttling option', () => {

            beforeEach(done => {
                const config = {
                    name: 'LOG_DEFAULT_NAME',
                    throttling: {}
                };
                suite.logger = Logger(config);
                done();
            });

            it('exposes full logger api', done => {
                expect(suite.logger.fatal).to.be.a.function();
                expect(suite.logger.error).to.be.a.function();
                expect(suite.logger.warn).to.be.a.function();
                expect(suite.logger.info).to.be.a.function();
                expect(suite.logger.debug).to.be.a.function();
                expect(suite.logger.trace).to.be.a.function();
                done();
            });
        });
    });

});