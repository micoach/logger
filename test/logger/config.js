'use strict';

const Code = require('code');
const Lab = require('lab');
const lab = exports.lab = Lab.script();
const {describe, it, beforeEach} = lab;

const {expect} = Code;

const Logger = require('../..');

describe('Logger', () => {

    const suite = {};

    beforeEach(done => {

        suite.config = {
            name: 'default',
            level: 'trace',
            file: {
                period: '1d',
                count: 3,
                basePath: '/var/log/tango'
            }
        };
        Logger(suite.config);
        done();
    });

    describe('.config', () => {

        it('exposes a config reader', done => {
            const logger = Logger(suite.config);
            expect(logger.config).to.include(suite.config);
            done();
        });

        it('extends default config', done => {
            const config = { file: false };
            const expected = Object.assign({}, suite.config, config);

            const logger = Logger(config);
            expect(logger.config).to.include(expected);
            done();
        });

        it('extends and merge nested default config', done => {
            const expected = Object.assign({}, suite.config);
            expected.file.count = 10;
            const logger = Logger({ file: { count: 10 } });
            expect(logger.config).to.include(expected);
            done();
        });
    });

    describe('set config via constructor', () => {

        it('extends default config', done => {
            const config = { file: false };
            const expected = Object.assign({}, suite.config, config);

            const logger = Logger(config);
            expect(logger.config).to.include(expected);
            done();
        });

    });
});