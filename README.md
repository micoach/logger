# Logger

## Overview
This repository provides logging interface for tango projects.

## Test
* `npm run check` runs all checkers and tests

## Installation
* `npm install logger` install logger

## Usage
1. Configure the logger **befor** you require the internal modules:

```js
const logger = require('logger')(config); // for config syntax look further
```

Example:

```js
const config = require('config');
const logger = require('logger')(config.logger);
const runtime = require('./lib/runtime');

runtime.run(config);
...
```

2. Then use the logger from the internal modules like that:

```js
const logger = require('logger');
```

Example:
```js
logger.info('Runtime');
results in:
{"name":"runtime-plugin-context","hostname":"localhost","pid":83743,"level":20,"msg":"Runtime","time":"2016-07-12T15:06:36.160Z","v":0}
```

OR provide a context to be additionaly printed:

```js
const logger = require('logger')('runtime');
```

Example:
```js
logger.info('Runtime')
results in:
{"name":"runtime-plugin-context","hostname":"localhost","pid":83743,"context":"runtime","level":20,"msg":"Runtime","time":"2016-07-12T15:06:36.160Z","v":0}
```


### Configuration

```
<name>
string
default: default
```

```
<level>
one of: info, warn, error, debug, trace
default: trace
```

```
<file>
object
default: {
    period: '1d',
    count: 3,
    basePath: '/var/log/tango'
}
```

```
<loggers>
array containing: 'stdout', 'file'
default: ['stdout']
```

```
<fields>
object containing any other fields are added to all log records as is
default: undefined
```

```
<throttling>
object 
example: {
    maxConcurrent: 1,//How many requests can be running at the same time
    highWater: 5 // Queue size, in case of overloaded queue other requests are skipped
}
default: undefined // unlimited 
```

```js
const logger = require('logger')({
    name,
    level,
    file,
    loggers
});
```

Example:
```js
{
    name: 'runtime-plugin-context',
    level: 'debug',
    loggers: ['file'],
    fields: { userId:101 },
    throttling: {
        maxConcurrent: 1,
        highWater: 5
    }
}
```

Usage in code:

```js
logger.fatal('fatal message');

logger.error('error message');

logger.warn('warn message');

logger.info('info message');

logger.debug('debug message');

logger.trace('trace message');
```