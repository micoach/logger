module.exports = function(config){
    config.set({
        files: [
            'test/**/*.js',
            { pattern: 'lib/**/*.js', included: false, mutated: true },
            { pattern: 'index.js', included: false, mutated: true }],
        testFramework: 'lab',
        testRunner: 'lab',
        reporter: ['progress', 'clear-text', 'dots', 'html', 'event-recorder'],
        coverageAnalysis: 'perTest',
        plugins: ['stryker-lab-runner', 'stryker-html-reporter']
    });
};