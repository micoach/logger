'use strict';

const path = require('path');
const {STDOUT,FILE} = require('./logger-types');
const LOGFILE_EXT = '.log';

module.exports = (config) => {

    const streamsConfig = [];

    if (config.loggers.includes(STDOUT)) {
        streamsConfig.push({
            level: config.level,
            stream: process.stdout
        });
    }

    if (config.loggers.includes(FILE)) {

        const logfile = path.format({
            dir: config.file.basePath,
            name: config.name,
            ext: LOGFILE_EXT
        });

        streamsConfig.push({
            level: config.level,
            path: logfile
        });
    }

    return streamsConfig;
};