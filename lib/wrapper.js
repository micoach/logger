'use strict';

const { merge } = require('lodash');
const Logger = require('./logger');
const { STDOUT } = require('./logger-types');
const Bottleneck = require('bottleneck');
const LOG_DEFAULT_ROTATE_DAYS = 1;
const LOG_DEFAULT_ROTATE_FILES_COUNT = 3;
const LOG_DEFAULT_ROTATE_BASE_FOLDER = '/var/log/tango';
const LOG_DEFAULT_LEVEL = 'trace';
const LOG_DEFAULT_NAME = 'default';
const BOTTLENECK_STRATEGY = Bottleneck.strategy.OVERFLOW;
const BOTTLENECK_MINTIME = 0;
const BOTTLENECK_DEFAULT_MAXCONCURRENT = 1;
const BOTTLENECK_DEFAULT_MAXQUEUE = 5;

const config = {
    name: LOG_DEFAULT_NAME,
    level: LOG_DEFAULT_LEVEL,
    loggers: [STDOUT],
    file: {
        period: LOG_DEFAULT_ROTATE_DAYS + 'd',
        count: LOG_DEFAULT_ROTATE_FILES_COUNT,
        basePath: LOG_DEFAULT_ROTATE_BASE_FOLDER
    }
};

let defaultLogger;

const createChild = (context, config) => {
    const logger = Logger.createChild(context, config);
    return wrapLogger(logger, config);
};

const wrapLogger = (logger, config) => {
    const wrapperFn = (context) => createChild(context, config);
    let limiter;
    if (config.throttling) {
        const maxConcurrent = config.throttling.maxConcurrent || BOTTLENECK_DEFAULT_MAXCONCURRENT;
        const highWater = config.throttling.highWater || BOTTLENECK_DEFAULT_MAXQUEUE;
        limiter = new Bottleneck.Cluster(maxConcurrent, BOTTLENECK_MINTIME, highWater, BOTTLENECK_STRATEGY);
    }
    Object.defineProperty(wrapperFn, 'config', {
        get() {
            return config;
        }
    });
    Object.defineProperty(wrapperFn, 'defaultLogger', {
        get() {
            return logger;
        }
    });

    ['fatal', 'error', 'warn', 'info', 'debug', 'trace'].forEach(fn => {
        wrapperFn[fn] = (...args) => {
            if (limiter) {
                limiter.key(fn).submit((cb) => {
                    wrapperFn.defaultLogger[fn](...args);
                    cb();
                });
            } else {
                wrapperFn.defaultLogger[fn](...args);
            }
        };
    });
    return wrapperFn;
};

const Wrapper = (configOrContext) => {
    if (typeof configOrContext === 'string') {
        return createChild(configOrContext, config);
    }
    merge(config, configOrContext);
    if (!defaultLogger) {
        defaultLogger = Logger.create(config);
    }
    return wrapLogger(defaultLogger, config);
};

module.exports = Wrapper;
