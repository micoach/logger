'use strict';

const bunyan = require('bunyan');
const streams = require('./streams');

const Logger = {

    create(config) {
        const options = {
            name: config.name,
            streams: streams(config)
        };
        if (config.fields) {
            Object.keys(config.fields)
                .forEach((key) => {
                    options[key] = config.fields[key];
                });
        }
        return bunyan.createLogger(options);
    },
    createChild(context, config) {
        return this.create(config).child({ context });
    }

};

module.exports = Logger;